import React from "react";

import ContactPage from "../../Components/ContactPage/ContactPage";
import AddContact from "../../Components/AddContact/AddContact";
import './MainPage.css';

function MainPage() {
    return (
            <div className="Main_Page">
                <h1 className="Contact_Page">Contact Page</h1>
                <div className="Contact_Page_Header">
                    <AddContact />
                </div>
                <ContactPage />
            </div>
    );
}

export default MainPage;
