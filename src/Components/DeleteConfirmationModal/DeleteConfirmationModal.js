import { useMutation } from '@apollo/client';

import './DeleteConfirmationModal.css';
import { DeleteContactById } from '../../GraphQL/Mutation/DeleteContactById';
import { GetContactList } from '../../GraphQL/Queries/GetContactList';


const DeleteConfirmationModal = ({openModal, setOpenModal, contact}) => {
    const [deleteContact] = useMutation(DeleteContactById);
    const handleOnDelete = () => {
        deleteContact( {
            variables: { id: contact.id},
            refetchQueries: [{query: GetContactList}]
        });
        setOpenModal(false);
    };

    if(!openModal) return null;
    return (
        <div className="Modal_Container">
            <h5>Are you sure you want to delete {contact.first_name} ?</h5>
        
            <button className="No_Button" onClick={() => setOpenModal(false)}>No</button>
            <button className="Yes_Button" onClick= {handleOnDelete}>Yes</button>
        </div>
    )
}

export default DeleteConfirmationModal;