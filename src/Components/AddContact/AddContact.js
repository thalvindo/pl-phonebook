import React, { useState } from "react";

import AddContactModal from "../AddContactModal/AddContactModal";
import './AddContact.css';

function AddContact () {
    const [openModal, setOpenModal] = useState(false);
    return (
        <div className="Add_Contact_Container">
            <div>
                <button className="Add_Contact_Button" onClick={() => setOpenModal(true)}>Add Contact</button>
                <AddContactModal open={openModal} onClose={() => setOpenModal(false)} />
            </div>
        </div>
    )
}

export default AddContact;