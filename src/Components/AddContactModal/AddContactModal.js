import { useMutation } from '@apollo/client';
import React, { useState } from "react";
import { isEmpty } from 'lodash';

import './AddContactModal.css';
import { AddContactWithPhones } from '../../GraphQL/Mutation/AddContactWithPhones';
import { GetContactList } from '../../GraphQL/Queries/GetContactList';

function AddContactModal({open, onClose}) {
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [phoneNumber1, setPhoneNumber1] = useState();
    const [phoneNumber2, setPhoneNumber2] = useState();
    const [addContact] = useMutation(AddContactWithPhones);
    
    const addPhone = (temporaryPhones, phoneNumber) => {
        if(!isEmpty(temporaryPhones)) {
            temporaryPhones = ([...temporaryPhones, {number: phoneNumber}])
        }
        if(isEmpty(temporaryPhones)) {
            temporaryPhones = ([{number: phoneNumber}])
        }
        return temporaryPhones;
    };

    const handleAddPhoneNumber = () => {
        let temporaryPhones = [];
        temporaryPhones = addPhone(temporaryPhones, phoneNumber1);
        if(!isEmpty(phoneNumber2)){
            temporaryPhones = addPhone(temporaryPhones, phoneNumber2);
        }
        return temporaryPhones;
    };

    const handleOnClickSubmit = () => {
        const phones = handleAddPhoneNumber();
                
        addContact( {
            variables: { first_name: firstName, last_name: lastName, phones: phones},
            refetchQueries: [{query: GetContactList}]
        });
    };

    if(!open) return null;
    return (
        <div className="Add_Contact_Modal">
            <div className="Modal_Container">
            <div className='Close_Modal_Container'>
                <div onClick={onClose} className="Close_Modal_Button">X</div>
            </div>
            <form onSubmit={(event) => { 
                event.preventDefault();

                setPhoneNumber1('');
                setPhoneNumber2('');
                setFirstName('');
                setLastName('');
                onClose();
            }} 
            className="Add_Contact_Form"
            >
                <div>
                    <label>first name</label>
                    <input type="text" required value={firstName} onChange={(e) => setFirstName(e.target.value)} id="first_name" />
                    <label>last name</label>
                    <input type="text" required value={lastName} onChange={(e) => setLastName(e.target.value)} id="last_name" />
                    <label>phone 1</label>
                    <input type="text" required value={phoneNumber1} onChange={(e) => setPhoneNumber1(e.target.value)} id="phone1" />
                    <label>phone 2</label>
                    <input type="text" value={phoneNumber2} onChange={(e) => setPhoneNumber2(e.target.value)} id="phone2" />
                    <button type="submit" onClick={handleOnClickSubmit}>Submit</button>
                </div>
            </form>
            </div>
        </div>
    );
}

export default AddContactModal;