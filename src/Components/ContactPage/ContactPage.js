
import {
  useQuery,
  useLazyQuery
} from "@apollo/client";
import React, { useEffect, useState } from "react";
import { isEmpty } from 'lodash';

import ContactBox from "../ContactBox/ContactBox";
import { GetContactList } from "../../GraphQL/Queries/GetContactList";
import './ContactPage.css'
import { GetContactListById } from "../../GraphQL/Queries/GetContactListById";

const ContactPage = () => {
  const [page, setPage] = useState(0);
  const [contacts, setContacts] = useState([]);
  const [searchedKeyword, setSearchedKeyword] = useState('');
  const PAGE_SIZE = 10;

  const [getContactListById, {data: searchedData, error}] = useLazyQuery(GetContactListById, {
    variables: {
      id: searchedKeyword
    }
  });

  const { data:contactData, loading } = useQuery(GetContactList, {
    variables: {
      limit: PAGE_SIZE,
      offset: page * PAGE_SIZE
    },
    pollInterval: 100
  });

  useEffect(() => {
      if (contactData) {
        setContacts(contactData.contact);
      }
      if (!isEmpty(searchedData)) {
        setContacts([searchedData.contact_by_pk]);
      }
  }, [contactData, searchedData]);

  const handleDisabledNextButton = () => {
    if(contactData.contact.length < 10) {
      return true;
    }
    return false;
  };

  console.log(contacts);
  console.log('a',searchedData);
  
  console.log(error);
  const handleSearchButton = () => {
    getContactListById();
    // setSearchedKeyword();
  }
    if(loading) return null;
    return (
        <div>
          <div className="Search_Button">
            <input type="number" value={searchedKeyword} onChange={(e)=> setSearchedKeyword(e.target.value)} />
            <button onClick={() => handleSearchButton()}>Search</button>
          </div>
          {contacts ? contacts.map((contact) => (
            <ContactBox contact={contact} />
          )) : <></>}
          <nav className="Pagination_Navigator">
            <button disabled={!page} onClick={()=> setPage(prev => prev - 1)}>Previous</button>
            <span className="Page_Indicator">Page {page + 1}</span>
            <button disabled={handleDisabledNextButton()} onClick={()=> setPage(prev => prev + 1)}>Next</button>
          </nav>
        </div>
    );

}

export default ContactPage;