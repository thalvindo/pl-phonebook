import React, { useState } from "react";

import FavoriteStar from '../../Assets/FavoriteStar/FavoriteStar.png';
import FavoritedStar from '../../Assets/FavoritedStar/FavoritedStar.png'
import './ContactBox.css';
import { handleOnClickFavorite } from "./UseContactBox";
import DeleteConfirmationModal from "../DeleteConfirmationModal/DeleteConfirmationModal";

const ContactBox = ({contact}) => {
  const [isFavorite, setIsFavorite] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);

    return(
      <div className="Contact_Box_Information">
      {contact ? 
        <div>
        <button className="Delete_Button" onClick={() => setOpenDeleteModal(true)}>Delete</button>
        <DeleteConfirmationModal openModal={openDeleteModal} setOpenModal={setOpenDeleteModal} contact={contact}/>
        <div onClick={() => handleOnClickFavorite(contact, isFavorite, setIsFavorite)} className="Favorite_Button">
          {isFavorite 
          ? <img src={FavoritedStar} alt="FavoritedStar" className="Favorite_Star"/>
          : <img src={FavoriteStar} alt="FavoriteStar" className="Favorite_Star"/>
          }
        </div>
        <h2 className="Contact_Name">{contact.first_name} {contact.last_name}</h2>
        <h3>Phone Number: </h3>
        {contact.phones.map((phone) => {
          return <h3>{phone.number}</h3>
        })}
      </div>
      : <></>
      }
      </div>
    )
};

export default ContactBox;