
export const handleOnClickFavorite = (contact, isFavorite, setIsFavorite) => {
    isFavorite ? setIsFavorite(false) : setIsFavorite(true)
};
