import { gql } from "@apollo/client";

export const GetContactList = gql`
query GetContactList (
  $limit: Int, 
  $offset: Int, 
) {
contact(
    limit: $limit, 
    offset: $offset
){
  created_at
  first_name
  id
  last_name
  phones {
    number
  }
}
}

`;